package xysoft.im;

import xysoft.im.app.Launcher;

/** 
 * 测试账户（密码都是1）：
wangxin@vm_0_4_centos
xuanji@vm_0_4_centos
zeyu@vm_0_4_centos
chenxiao@vm_0_4_centos
zhangyun@vm_0_4_centos
test1@vm_0_4_centos
test2@vm_0_4_centos
test3@vm_0_4_centos
test4@vm_0_4_centos
 */

public class StartUp {

	public static void main(String[] args)
    {
        Launcher launcher = new Launcher();
        launcher.launch();
    }

}
